<?php
/**
 * Created by PhpStorm.
 * User: dubinsky
 * Date: 15.9.12
 * Time: 11:49
 */

namespace slimsky\dbutils;

class DatabaseService {

    /**
     * @var \mysqli
     */
    private $db;

    private $db_address;

    private $db_user;

    private $db_pwd;

    private $db_database;

    private $db_charset;


    public function __construct($settings) {
        $this->db_address     = $settings['host'];
        $this->db_user        = $settings['user'];
        $this->db_pwd         = $settings['pw'];
        $this->db_database    = $settings['database'];
        if (in_array('charset', $settings)) {
            $this->db_charset = $settings['charset'];
        } else {
            $this->db_charset = 'utf8';
        }

        $this->db = new \mysqli();
        $this->Connect();
    }

    public function Connect() {
        if ($this->IsConnected())
            return;
        $this->db->connect( $this->db_address,
                                $this->db_user,
                                $this->db_pwd,
                                $this->db_database);
        $this->db->set_charset($this->db_charset);
    }

    /**
     * Returns \mysqli object and tries reconnect if old one is disconnected due to timeout
     *
     * @return \mysqli
     */
    public function getDB() {
        if(!$this->IsConnected()) {
            error_log("RECONNECT TO DB");
            $this->Connect();
        }
        return $this->db;
    }

    /**
     * @return bool true if current \mysqli object is connected
     */
    public function IsConnected() {
        if (mysqli_ping($this->db) === TRUE) {
            return true;
        }
        return false;
    }

    /**
     * Checks if mysqli is connected and returns last inserted id
     * returns null if it was disconnected
     *
     * @return mixed null or last insert id
     */
    public function getLastInsertId()
    {
        if (!$this->IsConnected())
            return null;
        return $this->getDB()->insert_id;
    }
}